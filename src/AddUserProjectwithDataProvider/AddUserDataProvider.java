package AddUserProjectwithDataProvider;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class AddUserDataProvider  {
	WebDriver driver;
	
	@BeforeTest
public void launchBrowser()
	{
		System.setProperty("webdriver.firefox.marionette", "geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://localhost:8888/connect2tech.in-Selenium-Automation-Java-1.x/users/add;jsessionid=rqumizx6t3kzk4oqebe2wev6");

	}
	

	
	 @DataProvider
	 public String[][] userdata(){String data [][] = new String[][] {
			
			{ "Ram", "abc1@gmail.com", "Priyan123", "Priyan123", "Wakad" }, 
			
			{ "John", "john@gmail.com", "Priyan123", "Priyan123", "Pimple gurav" },
				
			{ "Tom", "Tom@gmail.com", "Priyan123", "Priyan123", "Wakad" }
			
		};
		
		return data;
	}
	 
	 
//	@Test
//	public void assert1() {
//		String expected = "HomePage";
//
//		String titleOfPage = driver.getTitle();
//		System.out.println(titleOfPage);
//		
//		Assert.assertEquals(titleOfPage, expected);
//	}
		
	
	@Test(dataProvider= "userdata")
	public void addUser(String name1, String email1, String pwd1, String pwd2,String address)
	{
	
		WebElement name = driver.findElement(By.xpath(".//*[@id='name']"));
		name.clear();
	    name.sendKeys(name1);
	    
	    WebElement email = driver.findElement(By.name("email"));
		email.clear();
		email.sendKeys(email1);
		
		WebElement pwd = driver.findElement(By.id("password")); 
		pwd.sendKeys(pwd1);
		
		WebElement repwd = driver.findElement(By.name("confirmPassword")); 
		repwd.sendKeys(pwd2);
		
		WebElement addr = driver.findElement(By.id("address"));
		addr.clear();
		addr.sendKeys(address);
		
		
		WebElement newsletter = driver.findElement(By.id("newsletter"));   //doubt : if newsleeter is selected and dont want to select ,How to unselect it 
			newsletter.click();
			if(newsletter.isSelected())
			{
			newsletter.click();
			}
	
			List<WebElement> f1 = driver.findElements(By.name("framework"));
			if(!f1.isEmpty()){
				for(int i=0;i<f1.size();i++){
					 f1.get(i).click();
				}
				}
			
			List<WebElement> ra =driver.findElements(By.xpath(" //*[@id='userForm']/div[8]/div/label[1]"));    // Radio button
			System.out.println("this is messgae");
			ra.get(0).click();
			List<WebElement> ra2 =driver.findElements(By.xpath(" .//*[@id='userForm']/div[8]/div/label[2]"));
			ra2.get(0).click();
			
			
			List<WebElement> NumberRadio =driver.findElements(By.xpath(".//*[@id='userForm']/div[9]/div/label[5]"));     //Radio button for 5th Selection on radio button .
			System.out.println("Number 5 selected ");
			NumberRadio.get(0).click();
			
			Select dropdown= new Select (driver.findElement(By.name("country")));
			System.out.println("REached dropdown");
			//dropdown.deselectAll();
			dropdown.selectByVisibleText("United Stated");
			
			WebElement submit =driver.findElement(By.xpath(".//*[@id='userForm']/div[12]/div/button"));
			System.out.println("Click on submit");
			submit.click();
			
			WebElement link =driver.findElement(By.linkText("Add User"));
			link.click();
	
	}

	
		
	@Test()
	public void javaSkill(){
		}


         
}
		



