import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


	
public class Alertexample { 
	public static WebDriver driver;

static String url = "file:///C:/Users/nisha/workspace/JavaProject/LocatingMultipleElements.html";

public static void main(String str[]) {
	System.setProperty("webdriver.firefox.marionette",
			"D:/nchaurasia/Automation-Architect/SeleniumAutomationProject/drivers/geckodriver.exe");

	// System.setProperty("webdriver.chrome.driver", "");
	driver = new FirefoxDriver();
	driver.get(url);
	WebElement bt = driver.findElement(By.name("theButton"));
	bt.click();
}
	
	public void testSimpleAlert()
	{
		driver.findElement(By.id("simple")).click();
		TargetLocator target =driver.switchTo();
		Alert al =target.alert();
		al.accept();
		}
}
