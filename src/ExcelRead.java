import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.extractor.XSSFExportToXml;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelRead {
	public static void main(String[] args) throws IOException{
		
		readExcel();
	}

	 static void readExcel() throws IOException {
		 //File f1 =new File("UserData.xlsx");
		 
		
			FileInputStream fis = new FileInputStream("UserData.xlsx");
			XSSFWorkbook wb = new XSSFWorkbook(fis);

			XSSFSheet  sheet = wb.getSheet("Sheet1");
			
			Iterator<Row>  rows = sheet.iterator();
			while(rows.hasNext()){
				Row row = rows.next();
				
				Iterator<Cell>  cells  = row.iterator();
				
				while(cells.hasNext()){
					Cell cell = cells.next();
					
					System.out.println("cell="+cell);
				}
				
			}
		
	}
}
