import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class framesExample {
	public static WebDriver driver;
	static String url="https://seleniumhq.github.io/selenium/docs/api/java/";
	public static void main(String str[]) {
		System.setProperty("webdriver.firefox.marionette",
				"D:/nchaurasia/Automation-Architect/SeleniumAutomationProject/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get(url);
		List<WebElement> cnt = driver.findElements(By.tagName("frame"));
		int p =cnt.size();
	        
		System.out.println(+p);
		
		driver.switchTo().frame("packageListFrame");
		System.out.println("Reached 1 ");
		String ttle =driver.getTitle();
		System.out.println("Title " +ttle);
		String s= driver.getCurrentUrl();
		System.out.println(s);
		WebElement w= driver.findElement(By.xpath("//div[@class='indexContainer']/ul/li[5]"));
		w.click();
		
		driver.switchTo().parentFrame(); //Back to parent frame
		
		
		driver.switchTo().frame("packageFrame");
		System.out.println("Reachd 2");
		String s1=driver.getTitle();
		System.out.println(s1);
		
		String s2 =driver.getCurrentUrl();
		System.out.println(s2);
		WebElement frame2 =driver.findElement(By.xpath("/html/body/div/ul[1]/li[10]/a"));
		frame2.click();
		System.out.println("Clicked on WebDriver");
		
		driver.switchTo().parentFrame();
		
		driver.switchTo().frame("classFrame");
		driver.getTitle();
		System.out.println("Reached frame3 click on Firefoxdriver");
		
		WebElement frame3=driver.findElement(By.xpath("/html/body/div[4]/div[1]/ul/li/dl[2]/dd/a[4]"));
		frame3.click();
		
		
		
		
		//driver.switchTo().frame("//div[@class='indexContainer']");
		
		
		
		

}
}